﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tank : MonoBehaviour
{
    [SerializeField]
    int maxHp;
    [SerializeField]
    int curHp;

    void Init()
    {
        maxHp = 100;
        curHp = 100;
    }

    void Awake()
    {
        Init();
    }

    public void TakeDamage(int damage)
    {
        curHp -= damage;
        if(curHp <= 0)
        {
            Death();
        }
    }

    public void Death()
    {
        Destroy(this.gameObject);
    }
}
