﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	public GameObject explosion;
    int damage = 20;

    void OnTriggerEnter(Collider col)
    {
        GameObject e = Instantiate(explosion, this.transform.position, Quaternion.identity);
        Destroy(e, 1.5f);
        Destroy(this.gameObject);

        if (col.CompareTag("Enemy"))
        {
            col.GetComponent<Tank>().TakeDamage(damage);
        }
        if (col.CompareTag("Player"))
        {
            col.GetComponent<Tank>().TakeDamage(damage);
        }
    }
}
