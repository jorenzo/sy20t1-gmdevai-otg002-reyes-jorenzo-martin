﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPath : MonoBehaviour
{
    public UnityEngine.AI.NavMeshAgent agent;
    public GameObject wpManager;
    GameObject[] wps;

    // Start is called before the first frame update
    void Start()
    {
        wps = wpManager.GetComponent<WaypointManager>().waypoints;
    }


    public void GoToHelipad()
    {
        //graph.AStar(currentNode, wps[0]);
        //currentWaypointIndex = 0;
        agent.SetDestination(wps[0].transform.position);
    }

    public void GoToRuins()
    {
        //graph.AStar(currentNode, wps[10]);
        //currentWaypointIndex = 0;
        agent.SetDestination(wps[10].transform.position);
    }

    public void GoToFactory()
    {
        //graph.AStar(currentNode, wps[8]);
        //currentWaypointIndex = 0;
        agent.SetDestination(wps[8].transform.position);
    }
}
